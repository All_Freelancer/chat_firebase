import 'package:flutter/material.dart';

//**************************** CLASS CHAT MESSAGE ******************************
class ChatMessage extends StatelessWidget {

  final Map<String, dynamic> data;

  //******************************** CONSTRUCT *********************************
  ChatMessage(this.data);

  //******************************* ROOT WIDGETS *******************************
  @override
  Widget build(BuildContext context) {
    
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 16.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage(data["senderPhotoUrl"]),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data["senderName"],
                  style: Theme.of(context).textTheme.subhead,
                ),
                Container(
                    margin: const EdgeInsets.only(top: 5.0),
                    child: data["imgUrl"] != null ?
                    Image.network(data["imgUrl"], width: 250.0,) :
                    Text(data["text"])
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
//******************************************************************************
